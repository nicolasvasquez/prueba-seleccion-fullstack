import mongoose from '../database';

const character = new mongoose.Schema({
  id: String,
  name: String,
  gender: String,
  culture: String,
  house: String,
  alive: Boolean,
  image: String,
  books: [String],
  allegiance: [String],
  title: [String],
  slug: String,
}, { strict: true });

export default mongoose.model('Characters', character);
