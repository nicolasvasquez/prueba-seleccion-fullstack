const queryToLimit = ({ limit }) => {
  if (limit) {
    return parseInt(limit, 10);
  }
  return 10;
};

const queryToOffset = ({ offset }) => {
  if (offset) {
    return parseInt(offset, 10);
  }
  return 0;
};

export { queryToLimit, queryToOffset };
