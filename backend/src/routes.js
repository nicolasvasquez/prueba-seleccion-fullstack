import { Router } from 'express';
import { get, getOne, post } from './controllers/characters';

const router = Router();

router.get('/characters', get);
router.get('/characters/:id', getOne);
router.post('/characters', post);

export default router;
