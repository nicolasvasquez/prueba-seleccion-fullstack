import { config } from 'dotenv';

config();

export const {
  CHARACTERS_URL,
  DB_CONNECTION,
  DB_PORT,
  DB_HOST,
  DB_DATABASE,
  DB_USERNAME,
  DB_PASSWORD,

} = process.env;
