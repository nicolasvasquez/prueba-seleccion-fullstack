import axios from 'axios';
import Character from '../models/characters';
import { queryToLimit, queryToOffset } from '../utils';
import { CHARACTERS_URL } from '../config';

const get = async (req, res) => {
  const limit = queryToLimit(req.query);
  const offset = queryToOffset(req.query);
  const characters = await Character.find({
    name: { $regex: `.*${req.query.name}.*` },
    house: { $regex: `.*${req.query.house}.*` },
  }).limit(limit).skip(offset);
  const count = await Character.count();
  res.set({ 'total-count': count }).send(characters);
};

const getOne = async (req, res) => {
  const character = await Character.findById(req.params.id);
  res.send(character);
};

const post = async (_, res) => {
  try {
    const { data } = await axios.get(CHARACTERS_URL);
    data.forEach((el) => {
      // Can manage exception passing other argument to create.
      Character.create(el);
    });
    res.send({ msg: 'OK!' });
  } catch (err) {
    res.status(500).send({ err });
  }
};

export { get, getOne, post };
