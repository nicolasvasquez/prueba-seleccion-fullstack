import mongoose from 'mongoose';
import {
  DB_CONNECTION, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_DATABASE,
} from '../config';

const URL = `${DB_CONNECTION}://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`;

mongoose.connect(URL, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set('debug', true);

export default mongoose;
