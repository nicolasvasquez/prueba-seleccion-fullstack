import express from 'express';
import cors from 'cors';
import router from './src/routes';

const app = express();

const corsOptions = {
  exposedHeaders: 'total-count',
};

app.use(express.json());
app.use(cors(corsOptions));
app.use(router);
app.listen(2000);
