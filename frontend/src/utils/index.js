const isLastItem = (index, array) => index === array.length;

const setOffset = (page, items = 10) => page * items;

export { isLastItem, setOffset };
