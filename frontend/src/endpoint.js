import axios from 'axios';

const base = 'http://localhost:2000';

export const getAllCharacters = (params) => axios.get(`${base}/characters`, { params });
export const getOneCharacter = (params) => axios.get(`${base}/characters/${params.id}`, { params });
