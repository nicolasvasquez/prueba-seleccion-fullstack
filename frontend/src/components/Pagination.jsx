import React from 'react';
import PropTypes from 'prop-types';

const Pagination = ({
  totalPages, page, onNext, onPrevious,
}) => {
  const setPagination = () => {
    const content = [];
    if (page !== 0) {
      content.push(
        <li className="relative block py-2 px-3 leading-tight bg-white border border-gray-300 border-r-0 ml-0 rounded-l hover:bg-gray-200">
          <button type="button" className="page-link" onClick={onPrevious} key="previous">
            Anterior
          </button>
        </li>,
      );
    }
    if (totalPages >= page) {
      content.push(
        <li className="relative block py-2 px-3 leading-tight bg-white border border-gray-300 rounded-r hover:bg-gray-200" key="next">
          <button type="button" className="page-link" onClick={onNext}>
            Siguiente
          </button>
        </li>,
      );
    }
    return content;
  };

  return (
    <div>
      <ul className="flex pl-0 list-none rounded my-2">{setPagination()}</ul>
    </div>
  );
};

Pagination.propTypes = {
  totalPages: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  onNext: PropTypes.func.isRequired,
  onPrevious: PropTypes.func.isRequired,
};

export default Pagination;
