import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Pagination from './Pagination';

const Table = ({
  columns, data, page, total, onChangePage,
}) => {
  const classTr = 'hover:bg-gray-200';
  const classTd = 'py-4 px-6 border-b border-gray-200';
  const classTh = 'py-4 px-6 font-bold uppercase text-sm text-gray-600 border-b border-gray-200';
  const col = columns.slice();
  const totalPages = Math.round(total / data.length);

  const setHead = () => {
    col.push({ name: 'Actions' });
    const head = col.map((el) => (
      <th className={classTh} key={el.name}>
        {el.name}
      </th>
    ));
    return <tr className="bg-gray-100">{head}</tr>;
  };

  const setRow = (row) => {
    const content = columns.map((field) => (
      <td className={classTd} key={field.key}>
        {row[field.key]}
      </td>
    ));
    content.push(
      <td className={classTd} key="actions">
        <Link to={`/view/${row.id}`}>
          <i className="fas fa-eye" />
        </Link>
      </td>,
    );
    return <tr className={classTr} key={row.name}>{content}</tr>;
  };

  const setBody = () => data.map((el) => setRow(el));

  const handlePrevious = () => {
    onChangePage(page - 1);
  };

  const handleNext = () => {
    onChangePage(page + 1);
  };

  const renderPagination = () => {
    if (data.length !== 0) {
      return (
        <Pagination
          page={page}
          totalPages={totalPages}
          onNext={handleNext}
          onPrevious={handlePrevious}
        />
      );
    }
    return null;
  };

  return (
    <div>
      <table className="text-left w-full border-collapse">
        <thead>{setHead()}</thead>
        <tbody className="text-lg text-grey">{setBody()}</tbody>
      </table>
      <div className="my-10 mx-10 float-right">{renderPagination()}</div>
    </div>
  );
};

Table.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  page: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
};

export default Table;
