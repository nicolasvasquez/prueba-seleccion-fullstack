import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getOneCharacter } from '../endpoint';

const View = ({ match }) => {
  const [character, setCharacter] = useState({});
  const fieldsToDisplay = ['gender', 'slug', 'rank', 'house', 'culture'];
  const listsToDisplay = ['title', 'books'];

  useEffect(() => {
    const get = async () => {
      const { data } = await getOneCharacter({ id: match.params.id });
      setCharacter(data);
    };
    get();
  }, [match.params.id]);

  const renderInformation = () => (
    fieldsToDisplay.map((key) => (
      <p className="text-gray-700 text-base capitalize" key={key}>
        <b className="ml-2 mr-1">{`${key}: `}</b>
        {character[key]}
      </p>
    )));

  const renderLists = () => {
    if (character.books) {
      return listsToDisplay.map((key) => {
        const info = character[key].map((value) => (
          <li className="ml-6" key={value}>
            {value}
          </li>
        ));
        return (
          <div className="text-gray-700 text-base capitalize" key={key}>
            <b className="ml-2 mr-1">{`${key}: `}</b>
            <ul>{info}</ul>
          </div>
        );
      });
    }
    return '';
  };

  const renderImage = () => {
    const src = character.image
      || 'https://www.salonclasico.cl/images/mujer-incognita.jpg';
    return <img src={src} alt="Character" />;
  };

  return (
    <div>
      <div className="my-4">
        <Link to="/">
          <button
            className="px-4 py-2 bg-blue-500 hover:bg-blue-600 text-white font-bold rounded"
            type="button"
          >
            <i className="fas fa-chevron-left mr-2" />
            Atras
          </button>
        </Link>
      </div>
      <div className="bg-white rounded-t-lg overflow-hidden p-4 p-10 flex justify-center">
        <div className="max-w-sm w-full lg:max-w-full lg:flex">
          <div className="border-t border-l border-r border-gray-400 lg:border-r-0 lg:border-b lg:border-gray-400 h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
            {renderImage()}
          </div>
          <div className="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
            <div className="mb-8">
              <p className="text-sm text-gray-600 flex items-center">
                <i className="fill-current text-gray-500 w-3 h-3 mr-2 fas fa-user" />
                {character.alive ? 'Alive' : 'Dead'}
              </p>
              <div className="text-gray-900 font-bold text-xl mb-2 capitalize">
                {character.name}
              </div>
              {renderInformation()}
              {renderLists()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

View.propTypes = {
  match: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.bool]),
  ).isRequired,
};

export default View;
