import React, { useState, useEffect } from 'react';
import Table from '../components/Table';
import { getAllCharacters } from '../endpoint';
import { setOffset } from '../utils';

const List = () => {
  const [items, setItems] = useState([]);
  const [page, setPage] = useState(0);
  const [total, setTotal] = useState(0);
  const [name, setName] = useState('');
  const [house, setHouse] = useState('');
  const columns = [
    {
      name: 'Name',
      key: 'name',
    },
    {
      name: 'House',
      key: 'house',
    },
  ];

  const changePage = (newPage) => {
    setPage(newPage);
  };

  const handleOnChangeName = (e) => {
    setName(e.currentTarget.value);
  };

  const handleOnChangeHouse = (e) => {
    setHouse(e.currentTarget.value);
  };

  const getData = async () => {
    const response = await getAllCharacters({
      offset: setOffset(page),
      name,
      house,
    });
    setTotal(parseInt(response.headers['total-count'], 10));
    setItems(response.data);
  };

  const handleSearch = () => {
    if (page !== 0) {
      setPage(0);
    } else {
      getData();
    }
  };

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  return (
    <div>
      <h1 className="text-3xl py-4 border-b mb-10">Personajes</h1>
      <div className="flex flex-wrap -mx-3 mb-6">
        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="grid-name"
          >
            Name
          </label>
          <input
            id="grid-name"
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
            type="text"
            placeholder="Name"
            value={name}
            onChange={handleOnChangeName}
          />
        </div>
        <div className="w-full md:w-1/3 px-3">
          <label
            className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
            htmlFor="grid-house"
          >
            House
          </label>
          <input
            id="grid-house"
            className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
            type="text"
            placeholder="House"
            onChange={handleOnChangeHouse}
          />
        </div>
        <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
          <button
            type="button"
            className="bg-blue-500 hover:bg-blue-600 mt-6 text-white font-bold py-3 px-4 rounded"
            onClick={handleSearch}
          >
            <i className="fas fa-search pr-2" />
            Buscar
          </button>
        </div>
      </div>
      <Table
        data={items}
        columns={columns}
        total={total}
        page={page}
        onChangePage={changePage}
      />
    </div>
  );
};

export default List;
